﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behavior/Avoidance")]
public class AvoidanceBehavior : FilterFlockBehavior
{
    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        Transform Player = GameObject.FindWithTag("Player").transform;
        
        //if no neighbor, return no adjustment
        if(context.Count == 0)
            return Vector2.zero;
        
        //add all points together and average
        Vector2 avoidanceMove = Vector2.zero;
        int nAvoid = 5;
        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            Vector2 closestPoint = item.gameObject.GetComponent<Collider2D>().ClosestPoint(agent.transform.position);
            if (Vector2.SqrMagnitude(closestPoint - (Vector2)agent.transform.position) < flock.SquareAvoidanceRadius * Player.localScale.x/2)
            {
                nAvoid++;
                avoidanceMove += ((Vector2)agent.transform.position-closestPoint);
            }
        }

        
        if (nAvoid > 0)
        {
            avoidanceMove /= nAvoid;
        }
        return avoidanceMove;
    }
}
