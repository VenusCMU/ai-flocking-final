﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Flock : MonoBehaviour
{
    public FlockAgent agentPrefab;
    public List<FlockAgent> agents = new List<FlockAgent>();
    public FlockBehavior behavior;

    [Range(10, 500)] public int startingCount = 250;
    const float AgentDensity = 0.08f;

    [Range(1f, 100f)] public float driveFactor = 10f;
    [Range(1f, 100f)] public float maxSpeed = 5f;
    [Range(1f, 10f)] public float neighborRadius = 1.5f;
    [Range(0f, 1f)] public float avoidanceRadiusMultiplier = 0.5f;

    float squareMaxSpeed;
    float squareNeighborRadius;
    float squareAvoidanceRadius;

    public float SquareAvoidanceRadius
    {
        get { return squareAvoidanceRadius; }
    }

    void Start()
    {
        squareMaxSpeed = maxSpeed * maxSpeed;
        squareNeighborRadius = neighborRadius * neighborRadius;
        squareAvoidanceRadius = squareNeighborRadius * avoidanceRadiusMultiplier * avoidanceRadiusMultiplier;

        for (int i = 0; i < startingCount; i++)
        {
            Vector3 randomDonut = Random.insideUnitCircle.normalized * Random.Range(5, 14);
            FlockAgent newAgent = Instantiate(
                agentPrefab,
                (randomDonut),
                Quaternion.Euler(Vector3.forward * Random.Range(0f, 360f)),
                transform
            );
            
            newAgent.transform.localScale = newAgent.transform.localScale*Random.Range(1f,2.5f);

            newAgent.name = name +"_"+ i.ToString();
            
            if (Random.Range(0f, 20f) >= 19)
            {
                newAgent.gameObject.transform.localScale *= 10;
                Debug.Log("The Big has spawn. " + newAgent.name);
            }
            
            newAgent.Initialize(this);
            agents.Add(newAgent);
        }
    }

    void Update()
    {
        Transform Player = GameObject.FindWithTag("Player").transform;
        foreach (FlockAgent agent in agents)
        {
            List<Transform> context = GetNearbyObjects(agent);

                //FOR DEMO ONLY
                //agent.GetComponentInChildren<SpriteRenderer>().color = Color.Lerp(Color.black, Color.red, context.Count / 6f);

                Vector2 move = behavior.CalculateMove(agent, context, this);
                move *= driveFactor;

                float SpeedScale = Player.localScale.x/2;

                Vector2 moveScale = move * SpeedScale;
                
                if (moveScale.sqrMagnitude > squareMaxSpeed * SpeedScale)
                {
                    moveScale = moveScale.normalized * maxSpeed * SpeedScale;
                }

                if (Player.localScale.x <= agent.transform.localScale.x)
                {
                    moveScale = moveScale.normalized * Player.localScale.x / agent.transform.localScale.x * maxSpeed* SpeedScale;
                }
                agent.Move(moveScale);
        }

        List<Transform> GetNearbyObjects(FlockAgent agent)
        {
            List<Transform> context = new List<Transform>();
            Collider2D[] contextColliders = Physics2D.OverlapCircleAll(agent.transform.position, neighborRadius);
            foreach (Collider2D c in contextColliders)
            {
                if (c != agent.AgentCollider)
                {
                    if (c.CompareTag("FlockAgent"))
                    {
                        context.Add(c.transform);
                    }
                }
            }

            return context;
        }
    }
}
