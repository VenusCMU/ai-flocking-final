﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerSingleton : MonoBehaviour {
    private static GameManagerSingleton _instance;
    public static GameManagerSingleton Instance { get { return _instance; } }

    public ushort GameState = 0; // 0 = Alive, 1 = Pause, 2 = Dead, 3 = Win
    public ushort Score = 0;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }
}
