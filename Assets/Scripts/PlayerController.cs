﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speed;
    [SerializeField] private KeyCode MovingUp;
    [SerializeField] private KeyCode MovingDown;
    [SerializeField] private KeyCode MovingLeft;
    [SerializeField] private KeyCode MovingRight;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private float cameraOffset = 2f;
    [SerializeField] private float cameraRefDamp = 1f;
    public Vector3 randomDonut { get; private set; } = Vector3.zero;

    // Update is called once per frame
    private void Update()
    {
        randomDonut = Random.insideUnitCircle.normalized * Random.Range(5, 7) * (Vector2)gameObject.transform.localScale + (Vector2)transform.position;
        if (Input.GetKey(MovingUp))
        {
            rb.AddForce(Vector2.up*speed*gameObject.transform.localScale.x);
        }
        if (Input.GetKey(MovingDown))
        {
            rb.AddForce(Vector2.down*speed*gameObject.transform.localScale.x);
        }
        if (Input.GetKey(MovingLeft))
        {
            rb.AddForce(Vector2.left*speed*gameObject.transform.localScale.x);
        }
        if (Input.GetKey(MovingRight))
        {
            rb.AddForce(Vector2.right*speed*gameObject.transform.localScale.x);
        }

        Camera.main.orthographicSize = Mathf.SmoothDamp(Camera.main.orthographicSize, gameObject.transform.localScale.x + cameraOffset, ref cameraRefDamp,10 *Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("FlockAgent"))
        {
            if (other.transform.localScale.x > gameObject.transform.localScale.x)
            {
                GameManagerSingleton.Instance.GameState = 2;
            }
            else
            {
                int receivedScore = Mathf.RoundToInt(other.transform.localScale.x);
                if (receivedScore >= 0)
                {
                    //Debug.Log(other.gameObject.name);
                    transform.localScale += new Vector3(other.transform.localScale.x/20, other.transform.localScale.y/20);
                    GameManagerSingleton.Instance.Score += (ushort)receivedScore;
                    other.gameObject.transform.localScale = gameObject.transform.localScale*Random.Range(.5f,2.5f);
                    if (Random.Range(0f, 20f) >= 20)
                    {
                        other.gameObject.transform.localScale *= 10;
                    }
                    other.transform.position = randomDonut;
                }
                else
                {
                    //Debug.Log(other.gameObject.name);
                    transform.localScale += new Vector3(other.transform.localScale.x/20, other.transform.localScale.y/20);
                    GameManagerSingleton.Instance.Score += (ushort)-receivedScore;
                    other.gameObject.transform.localScale = gameObject.transform.localScale*Random.Range(.5f,2.5f);
                    if (Random.Range(0f, 20f) >= 19)
                    {
                        gameObject.transform.localScale *= 5;
                        Debug.Log("The Big has spawn");
                    }
                    other.transform.position = randomDonut;
                }
            }
        }
    }
}
