﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "Flock/Behavior/Stay In Radius")]

public class StayInRadiusBehavior : FlockBehavior
{
    //public Vector2 center;
    private Transform Player;
    public float radius =15f;
    private float radiusScale = 15f;
    
    public override Vector2 CalculateMove(FlockAgent agent,List<Transform> context, Flock flock)
    {
        Player = GameObject.FindGameObjectWithTag("Player").transform;
        radiusScale = radius * Player.localScale.x;
        Vector2 centeroffset = (Vector2)Player.position - (Vector2)agent.transform.position;
        float t = centeroffset.magnitude/radiusScale;
        if (t <0.9f)
        {
            return Vector2.zero;
        }

        return centeroffset*t *t;
    }
}
